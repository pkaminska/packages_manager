require 'open-uri'
namespace :cp do
  # cp - custom_packages
  desc 'Download PACKAGE.txt file'
  task :get_info_file => :environment do
    dir = "#{Rails.root}/public/packages"
    FileUtils.mkdir(dir) unless Dir.exists?(dir)
    packages_file = open("https://cran.r-project.org/src/contrib/PACKAGES")
#    packages_file = File.read("#{Rails.root}/test/fixtures/files/PACKAGES_test.txt")
    f = File.new("#{Rails.root}/public/packages/PACKAGES.txt", 'w')
    f.puts packages_file.read
    f.close
    
    p 'PACKAGES.txt was saved'
  end
  
  desc 'Seed db with Package names'
  task :set_package_names => :environment do
    # set path to file - absolute path, could be changed to provide by user
    # f_path = "#{Rails.root}/public/packages/PACKAGES.txt" <--- this path to be used 
    f_path = "#{Rails.root}/public/packages/PACKAGESs.txt" # dummy path
    # truncate table - optional, or can be conditional
    query = "TRUNCATE TABLE `package_names`;"
    results = ActiveRecord::Base.connection.execute(query)
    p 'package_names table was truncated'
    
    if File.exist?(f_path)
      PackageName.seed_database(f_path)
      p 'PACKAGEST.txt was compiled into db'
    else 
      PackageName.seed_database
      p 'test file was compiled into db'
    end
    
  end
  
  desc 'Seed db with Package details'
  task :set_package_details => :environment do
    # if changed to one-to-many association, needs some conditions (validate uniqueness of :version) or truncate
    PackageName.create_package_details
    p 'Package details was added'
  end
  
end