class CreatePackageNames < ActiveRecord::Migration[5.1]
  def change
    create_table :package_names do |t|
      t.string :name
      t.string :full_name
      t.boolean :available, defaut: false
      
      t.timestamps
    end
  end
end
