class CreatePackageDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :package_details do |t|
      t.string :title
      t.text :description
      t.string :authors
      t.string :version
      t.string :maintainers
      t.string :license
      t.date :publication_date
      
      t.belongs_to :package_name, index: true
      
      t.string :package_url
            
      t.timestamps
    end
  end
end
