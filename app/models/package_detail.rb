class PackageDetail < ApplicationRecord
  belongs_to :package_name
  
  # validations
  validates :title, presence: true
  validates :version, presence: true
  # validates :authors, presence: true # can be changed into association
  # validates :maintainers, presence: true # can be changed into association
  validates :description, presence: true
  validates :license, presence: true # can be changed into association
  validates :publication_date, presence: true
  
  before_destroy :make_package_unavailable
  
  
  def self.create_from_hash(pn, hash)
    # can be moved to module with the same method in PackageName via respond_to? method
    if !hash.is_a?(Hash)
      logger.debug "Skipped: #{hash}"
    else
      a = self.create(
        package_name: pn, 
        title: hash[:title], 
        version: hash[:version], 
        authors: hash[:authors], 
        maintainers: hash[:maintainers] || hash[:maintainer], 
        description: hash[:description], 
        license: hash[:license], 
        publication_date: hash[:'date/publication'].to_datetime,
        package_url: PackageName::PACKAGES_SOURCE_URL + pn.full_name + '.tar.gz'
        )
      a.package_name.available! if a
      # logger.debug self.last.errors.inspect if self.last.errors.any?
      p "Added deatails to: #{pn.name}" # message for rake task, could be conditional, should be added to log file
    end
  end
  
  def make_package_unavailable
    pn = self.package_name
    pn.unavailable!
  end  
  
end
