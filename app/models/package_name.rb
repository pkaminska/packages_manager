class PackageName < ApplicationRecord
  BASE_PACKAGE_LIST_PATH = 'test/fixtures/files/PACKAGES_test.txt'
  PACKAGES_SOURCE_URL = 'https://cran.r-project.org/src/contrib/'
  has_one :package_detail, dependent: :destroy # can change to has_many if we want to maintain history of packages versions
  
  validates :name, presence: true
  validates :name, uniqueness: true
  
  scope :default, -> { order 'package_names.name ASC' }
  scope :available, -> { where available: true }  
  
  def self.seed_database(base_file_path = BASE_PACKAGE_LIST_PATH)
    file_content = File.read(base_file_path)
    p_array = file_content.split("\n\n")
    p_array.each do |p|
      p_hash = p.split("\n").map{|h| h1,h2 = h.split(': '); { h1.downcase.to_sym => h2 }}.reduce(:merge) #parserer => can be taken to  module
      PackageName.create_from_hash(p_hash)
    end
  end
  
  def self.create_from_hash(hash)
    if !hash.is_a?(Hash) && hash[:package].blank? && hash[:varsion].blank? # to go through loop -- need rake task info
      logger.debug "Skipped: #{hash}"
    else
      self.create(name: hash[:package], full_name: hash[:package]+'_'+hash[:version])
    end
  end
  
  def self.create_package_details
    self.all.each { |p| p.create_package_detail }
  end
  
  def available?
    available == true
  end
  
  def available!
    self.available = true
    self.save!
  end
  
  def unavailable!
    self.available = false
    self.save
  end
  
  ### package_detail handling methods
  
  def create_package_detail
    n = self.full_name
    url = PACKAGES_SOURCE_URL + n + '.tar.gz'
    # create an unzipped folder in tmp file
    Minitar.unpack(Zlib::GzipReader.new(File.open(open(url), 'rb')), 'tmp/packages')
    desc_content = File.read("tmp/packages/#{self.name}/DESCRIPTION")
    desc_content = desc_content.gsub(/\n\s+/, " ") # gsub to get rid of multilines with tabs
    d_array = desc_content.split("\n")
    d_hash = {}
    d_array.each do |d|
      dd = d.split(": ")
      d_hash[dd[0].downcase.to_sym] = dd[1]
    end
    PackageDetail.create_from_hash(self, d_hash)
    # Removing folder from tmp/packages -> route to oryginal tar.gz is saved in PackageDatail
    FileUtils.remove_dir("tmp/packages/#{self.name}")
  end
  
end
